

package cz.mendelu.pef.pjj_2019;

import cz.mendelu.pef.pjj_2019.Segmenty.Field;
import cz.mendelu.pef.pjj_2019.Segmenty.Route;
import cz.mendelu.pef.pjj_2019.Segmenty.Town;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

public class SaverTest {
    /**
     * @author xbobcik  @version: etapa 3
     */

    @Test
    public void ulozeniStavuHry (){

        Saver saver = new Saver();
        Field pole = new Field();
        Town mesto = new Town();
        Route cesta = new Route();

        Game g = new Game();
        g.Init(9);

        Player adam = new Player(7);
        Player betty = new Player(6);
        Player cecil = new Player(7);


        Card karta1 = new Card(cesta,cesta,pole,mesto,cesta);
        Card karta2 = new Card(cesta,cesta,cesta,cesta,cesta);
        Card karta3 = new Card(mesto,mesto,mesto,mesto,mesto);

        g.polozKartu(5,4,karta1,g.getMapa());
        g.polozKartu(3,4,karta2,g.getMapa());
        g.polozKartu(4,3,karta3,g.getMapa());

        g.players.add(adam);
        g.players.add(betty);
        g.players.add(cecil);


        saver.ulozeniStavuHry(g);

        boolean souborExistuje = false;
        File save = new File(saver.getPath());
        souborExistuje=save.exists();

        assertEquals(souborExistuje,true);




    }



}
