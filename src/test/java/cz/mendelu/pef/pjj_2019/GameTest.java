package cz.mendelu.pef.pjj_2019;

import cz.mendelu.pef.pjj_2019.Segmenty.Field;
import cz.mendelu.pef.pjj_2019.Segmenty.Route;
import cz.mendelu.pef.pjj_2019.Segmenty.Town;
import org.junit.Test;


import java.io.File;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class GameTest {


    /** @author xblahov1  @version: etapa 3 */
    @Test
    public void Init(){

        Game g = new Game();
        g.Init(9);
        assertEquals(2,g.players.size());
        assertEquals(g.getMapa()[4][4].getPolozena(),true); // pocatecni karta je polozena

        int pocetKaret = 0;
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (g.getMapa()[j][i].getPolozena() == true) pocetKaret++;
            }
            }
        assertEquals(pocetKaret,1); // jen jedna polozena karta na mape

    }

    /** @author xbobcik  @version: etapa 3 */
    @Test
    public void stranyPasuji() {
        Game hra = new Game();
        hra.Init(9);
        Field pole = new Field();
        Town mesto = new Town();
        Card po = new Card(pole,pole,pole,mesto,pole);
        Card pri = new Card(pole,pole,mesto,pole,pole);

        assertEquals(true, hra.stranyPasuji(po.getLeftSide(),pri.getRightSide()) );
        assertEquals(true, hra.stranyPasuji(po.getTopSide(),pri.getBottomSide()) );
    }

    /** @author xbobcik  @version: etapa 3 */
    @Test
    public void lzePolozitZVrchu(){
        Game hra = new Game();
        hra.Init(9);
        Field pole = new Field();
        Town mesto = new Town();
        Card k = new Card(pole,pole,pole,mesto,pole);
        Card l = new Card(pole,pole,mesto,pole,pole);

        System.out.println("\n " + hra.lzePolozitZVrchu(4,5,k,hra.getMapa()));
        System.out.println("\n " + hra.lzePolozitZVrchu(4,5,l,hra.getMapa()));
        //porovnat s kartou
        assertEquals(true, hra.lzePolozitZVrchu(4,5,k,hra.getMapa()));
        assertEquals(false, hra.lzePolozitZVrchu(4,5,l,hra.getMapa()));
        //do prazdna
        assertEquals(true, hra.lzePolozitZVrchu(4,3,k,hra.getMapa()));

    }

    /** @author xbobcik  @version: etapa 3 */
    @Test
    public void lzePolozitZeSpodu(){
        Game hra = new Game();
        hra.Init(9);
        Field pole = new Field();
        Town mesto = new Town();
        Card k = new Card(pole,pole,pole,mesto,pole);
        Card l = new Card(pole,pole,mesto,pole,pole);

        System.out.println("\n " + hra.lzePolozitZeSpodu(4,3,k,hra.getMapa()));
        System.out.println("\n " + hra.lzePolozitZeSpodu(4,3,l,hra.getMapa()));
        //porovnat s kartou
        assertEquals(true, hra.lzePolozitZeSpodu(4,3,k,hra.getMapa()));
        assertEquals(false, hra.lzePolozitZeSpodu(4,3,l,hra.getMapa()));
        //do prazdna
        assertEquals(true, hra.lzePolozitZeSpodu(3,4,k,hra.getMapa()));

    }

    /** @author xbobcik  @version: etapa 3 */
    @Test
    public void lzePolozitZPrava(){
        Game hra = new Game();
        hra.Init(9);
        Field pole = new Field();
        Town mesto = new Town();
        Route cesta = new Route();
        Card k = new Card(pole,cesta,pole,mesto,pole);
        Card l = new Card(cesta,pole,mesto,pole,pole);

        System.out.println("\n " + hra.lzePolozitZPrava(3,4,k,hra.getMapa()));
        System.out.println("\n " + hra.lzePolozitZPrava(3,4,l,hra.getMapa()));
        //porovnat s kartou
        assertEquals(true, hra.lzePolozitZPrava(3,4,k,hra.getMapa()));
        assertEquals(false, hra.lzePolozitZPrava(3,4,l,hra.getMapa()));
        //do prazdna
        assertEquals(true, hra.lzePolozitZPrava(5,4,k,hra.getMapa()));

    }

    /** @author xbobcik  @version: etapa 3 */
    @Test
    public void lzePolozitZLeva(){
        Game hra = new Game();
        hra.Init(9);
        Field pole = new Field();
        Town mesto = new Town();
        Route cesta = new Route();
        Card k = new Card(pole,cesta,pole,mesto,pole);
        Card l = new Card(cesta,pole,mesto,pole,pole);

        System.out.println("\n " + hra.lzePolozitZLeva(5,4,l,hra.getMapa()));
        System.out.println("\n " + hra.lzePolozitZLeva(5,4,k,hra.getMapa()));
        //porovnat s kartou
        assertEquals(true, hra.lzePolozitZLeva(5,4,l,hra.getMapa()));
        assertEquals(false, hra.lzePolozitZLeva(5,4,k,hra.getMapa()));
        //do prazdna
        assertEquals(true, hra.lzePolozitZLeva(5,5,k,hra.getMapa()));

    }

    /** @author xbobcik  @version: etapa 3 */

    @Test
    public void lzePolozitKartu() {
        Field pole = new Field();
        Town mesto = new Town();
        Route cesta = new Route();

        Game hra = new Game();
        hra.Init(9);
        Card karta = new Card(cesta,cesta,pole,mesto,cesta);
        Card karta2 = new Card(cesta,cesta,cesta,cesta,cesta);
        assertEquals(true ,hra.lzePolozitKartu(3,4,karta,hra.getMapa()));// z leva
        assertEquals(true ,hra.lzePolozitKartu(4,5,karta,hra.getMapa()));//z dola
        assertEquals(true ,hra.lzePolozitKartu(5,4,karta,hra.getMapa()));// z prava
        assertEquals(true ,hra.lzePolozitKartu(4,3,karta,hra.getMapa()));// z hora

        //do prazdna
        assertEquals(false ,hra.lzePolozitKartu(2,2,karta,hra.getMapa()));
    }
    /** @author xbobcik  @version: etapa 3 */
    @Test
    public void polozKartu() {
        Field pole = new Field();
        Route cesta = new Route();

        Game hra = new Game();
        hra.Init(9);

        Card karta2 = new Card(cesta,cesta,pole,pole,cesta);
        hra.polozKartu(6,5,karta2,hra.getMapa());// karta do prazdna se nevlozi
        hra.polozKartu(3,4,karta2,hra.getMapa());//karta vedle pocatecni
        int pocet=0;
        for (int i = 0; i < 9; i++) {
            System.out.println();
            for (int j = 0; j < 9; j++) {
                if (hra.getMapa()[i][j].getPolozena()) {
                    pocet++;
                }
            }
        }
        assertEquals(2,pocet);
        // dodelat / domyslet  pole pri vytvoreni obsahuje 9 objektu typu karta
        // vytvorenych bezparametrickym konstruktorem
        // nevim jak spocitat ze jsou tam jen 2 karticky [objekty typu Card, ktere jsme
        // tam dali my umyslne.. nebo vymyslet lepsi strukturu ]
        // assertEquals(2,hra.mapa.size()); test vyhodi chybu ocekava 2 dostal 9
        // jenze my vime ze mapa muze obsahovat 9x9 policek a z toho jsme na ni umystili 2 karty (pocatecni a kartu 2



    }
    /** @author xblahov1  @version: etapa 4 */
    @Test
    public void naplneniBalikuKaret(){
        int pocetKaret = 27;
        Game g = new Game();
        g.Init(9);
        g.naplneniBalikuKaret();
        assertEquals(pocetKaret, g.getCardsInDeck().size());

    }

    /** @author xblahov1  @version: etapa 3 */
    @Test
    public void vylosujKartu(){
        Game g = new Game();
        g.Init(9);
        g.naplneniBalikuKaret();
        int pocetPredLosovanim = g.getCardsInDeck().size();
        Card vylosovanaKarta = g.vylosujKartu();
        int pocetPoLosovani = g.getCardsInDeck().size();
        System.out.println(vylosovanaKarta + "\n" + "pocet karet pred losovanim: " + pocetPredLosovanim + " a po losovani: " + pocetPoLosovani);

        assertNotEquals(null, vylosovanaKarta); // karta neni null
        assertEquals(g.getCardsInDeck().contains(vylosovanaKarta),true); // vylosovana karta byla v baliku
        assertEquals(pocetPredLosovanim,pocetPoLosovani+1); // v baliku je po losovani o jednu kartu mene

    }


}