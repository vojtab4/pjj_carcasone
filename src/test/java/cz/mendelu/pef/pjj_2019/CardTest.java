package cz.mendelu.pef.pjj_2019;

import cz.mendelu.pef.pjj_2019.Segmenty.Field;
import cz.mendelu.pef.pjj_2019.Segmenty.Route;
import cz.mendelu.pef.pjj_2019.Segmenty.Town;
import org.junit.Test;

import static org.junit.Assert.*;

public class CardTest {

    @Test
    public void OtocitKartuDoPrava() {
        Field pole = new Field();
        Town mesto = new Town();
        Route cesta = new Route();

        Card karta = new Card(mesto, pole, pole, cesta, cesta);
        assertEquals(mesto, karta.getLeftSide());

        karta.OtocitKartuDoPrava();
        assertEquals(mesto, karta.getTopSide());
        assertEquals(cesta, karta.getLeftSide());

        karta.OtocitKartuDoLeva();
        assertEquals(pole, karta.getTopSide());
        assertEquals(mesto, karta.getLeftSide());
        assertEquals(cesta, karta.getBottomSide());
    }



}
