package cz.mendelu.pef.pjj_2019;

import org.junit.Test;

import static org.junit.Assert.*;

public class PlayerTest {

    @Test
    public void pridejFigurkuDoVylozenych() {
        int ocekavanyPocetPrvku=1;

        Player adam = new Player(7);
        Burglar pepa = new Burglar(1,1);
        adam.PridejFigurkuDoVylozenych(pepa);

        assertEquals(ocekavanyPocetPrvku, adam.getVylozeneFigurky().size());

        Burglar p2 = new Burglar(1,1);adam.PridejFigurkuDoVylozenych(p2);
        Burglar p3 = new Burglar(1,1);adam.PridejFigurkuDoVylozenych(p3);
        Burglar p4 = new Burglar(1,1);adam.PridejFigurkuDoVylozenych(p4);
        Burglar p5 = new Burglar(1,1);adam.PridejFigurkuDoVylozenych(p5);
        Burglar p6 = new Burglar(1,1);adam.PridejFigurkuDoVylozenych(p6);
        Burglar p7 = new Burglar(1,1);adam.PridejFigurkuDoVylozenych(p7);

        assertEquals(adam.MAXPOCETFIGUREK,adam.getVylozeneFigurky().size());
        Burglar p8 = new Burglar(1,1);adam.PridejFigurkuDoVylozenych(p8);
        assertEquals(adam.MAXPOCETFIGUREK,adam.getVylozeneFigurky().size());
    }






}