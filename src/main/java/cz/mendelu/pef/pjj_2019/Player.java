package cz.mendelu.pef.pjj_2019;

import java.util.ArrayList;
import java.util.Objects;

public class Player {
    public final int MAXPOCETFIGUREK = 7;
    private int pocetBoduUzavrene;
    private int pocetBoduNeuzavrene;
    private int aktualniPocetFigurek;
    private ArrayList<Figure> vylozeneFigurky = new ArrayList<Figure>(); // sem se budou davat figurky

    enum Barvy {cervena, modra, zelena, zluta}


    private Barvy barva;

    public void PridejFigurkuDoVylozenych(Figure kterouFigurku) {
        if (vylozeneFigurky.size() < MAXPOCETFIGUREK) {
            vylozeneFigurky.add(kterouFigurku);
        }
    }

    public Player(int aktualniPocetFigurek) {
        this.aktualniPocetFigurek = aktualniPocetFigurek;
    }


    public ArrayList<Figure> getVylozeneFigurky() {
        return vylozeneFigurky;
    }

    /**
     * @author xblahov1  @version: etapa 3
     */
    public void VybratBarvu(Barvy barva) {
        this.barva = barva;
    }

    /**
     * @author xblahov1  @version: etapa 3
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return Objects.equals(barva, player.barva);
    }

    /**
     * @author xblahov1  @version: etapa 3
     */
    // kazdy hrac bude mít jiný hashcode na základě barvy figurky
    @Override
    public int hashCode() {
        return Objects.hash(barva);
    }

    /**
     * @author xblahov1  @version: etapa 3
     */
    @Override
    public String toString() {
        return "Player with color " + barva + "\n" +
                "pocetBoduUzavrene=" + pocetBoduUzavrene + "\n" +
                "pocetBoduNeuzavrene=" + pocetBoduNeuzavrene + "\n" +
                "aktualniPocetFigurek=" + aktualniPocetFigurek + "\n" +
                "vylozeneFigurky=" + vylozeneFigurky +
                '}';
    }


}


