package cz.mendelu.pef.pjj_2019;

import cz.mendelu.pef.pjj_2019.Segmenty.Field;
import cz.mendelu.pef.pjj_2019.Segmenty.Route;
import cz.mendelu.pef.pjj_2019.Segmenty.Town;

import java.util.Objects;

public class Card {

    private Figure figurkaNaKarte;


    private Segment leftSide;
    private Segment rightSide;
    private Segment topSide;
    private Segment bottomSide;
    private Segment center;
    private boolean polozena;


//gettry //////////////////////////////////////////////////

    public Segment getLeftSide() {
        return leftSide;
    }
    public Segment getRightSide() {
        return rightSide;
    }
    public Segment getTopSide() {
        return topSide;
    }
    public Segment getBottomSide() {
        return bottomSide;
    }
    public Segment getCenter() {
        return center;
    }
    public boolean getPolozena() {
        return polozena;
    }
    //settery //////////////////////////////////////////////////

    public void setPolozena(boolean polozena) {
        this.polozena = polozena;
    }

    ///////////////////////////////////////////////////////


    public Card(Segment leftSide, Segment rightSide, Segment topSide, Segment bottomSide, Segment center) {
        this.leftSide = leftSide;
        this.rightSide = rightSide;
        this.topSide = topSide;
        this.bottomSide = bottomSide;
        this.center = center;
        this.polozena=false;
    }

    public Card(boolean polozena){
        this.polozena=polozena;

        this.leftSide = null;
        this.rightSide =null;
        this.topSide = null;
        this.bottomSide = null;
        this.center = null;
    }
    // v metode pouzita pomocna promenna do ktere se ulozi jedna z hran karticky..
    // a potom se postupne popreavaji a tim se karticka otoci
    public void OtocitKartuDoPrava(){
        Segment pomocny;
        pomocny=topSide;
        topSide=leftSide;
        leftSide=bottomSide;
        bottomSide=rightSide;
        rightSide=pomocny;
        pomocny=null;

    }

    public void OtocitKartuDoLeva(){
        Segment pomocny;
        pomocny=topSide;
        topSide=rightSide;
        rightSide=bottomSide;
        bottomSide=leftSide;
        leftSide=pomocny;
        pomocny=null;

    }


    /** @author xblahov1  @version: etapa 3 */
    @Override
    public boolean equals(Object c) {
        if (c == null || getClass() != c.getClass()) return false; // objekt je null nebo nemají stejne tridy
        Card card = (Card) c; // pretypovani na Card
        return
                Objects.equals(leftSide.getNazev(), card.leftSide.getNazev()) &&
                Objects.equals(rightSide.getNazev(), card.rightSide.getNazev()) &&
                Objects.equals(topSide.getNazev(), card.topSide.getNazev()) &&
                Objects.equals(bottomSide.getNazev(), card.bottomSide.getNazev()) &&
                Objects.equals(center.getNazev(), card.center.getNazev());
    }

    /** @author xblahov1  @version: etapa 3 */
    @Override
    public int hashCode() {
        return Objects.hash(leftSide, rightSide, topSide, bottomSide, center);
    }

    /** @author xblahov1  @version: etapa 3 */
    @Override
    public String toString() {
        return "Card{" + "\n" +
                "isPlacedOnMap=" + polozena + "\n"+
                "hasPlacedFigure=" + figurkaNaKarte + "\n"+
                "leftSide=" + leftSide.getNazev() + "\n"+
                "rightSide=" + rightSide.getNazev() + "\n"+
                "topSide=" + topSide.getNazev() + "\n"+
                "bottomSide=" + bottomSide.getNazev() + "\n"+
                "center=" + center.getNazev() +
                '}';
    }

    /** @author xblahov1  @version: etapa 3 */
    // pro testovani metod toString, hashCode a equals
    public static void main(String[] args) {
        Field pole = new Field();
        Town mesto = new Town();
        Route cesta = new Route();
        Card karta1 = new Card(mesto, pole, pole, cesta, pole);
        Card karta2 = new Card(mesto, pole, pole, cesta, pole);

        System.out.println(karta1.equals(karta2));
        System.out.println(karta1.toString());
        System.out.println(karta1.hashCode());  System.out.println(karta2.hashCode()); // maji stejny hashcode


    }



}
