package cz.mendelu.pef.pjj_2019;

import cz.mendelu.pef.pjj_2019.Segmenty.Field;
import cz.mendelu.pef.pjj_2019.Segmenty.Monastery;
import cz.mendelu.pef.pjj_2019.Segmenty.Route;
import cz.mendelu.pef.pjj_2019.Segmenty.Town;

import java.io.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class Game implements Serializable{

    private ArrayList<Card> cardsInDeck;
    public ArrayList<Player> players;
    public Card[][] mapa;

    public Field pole = new Field();
    public Town mesto = new Town();
    public Route cesta = new Route();

    public Card[][] getMapa() {
        return mapa;
    }



    // inicializační metoda, která vytvoří počáteční kartičku kterou vloží do středu mapy a 2 hráče
    // # velikostMapy = nejlepe licha
    /**
     * @author xblahov1  @version: etapa 3
     */
    public void Init(int velikostMapy) {
        cardsInDeck = new ArrayList<Card>();
        players = new ArrayList<Player>();
        mapa = new Card[velikostMapy][velikostMapy];

        Card pocatecniKarta = new Card(cesta, cesta, mesto, pole, cesta); // init karta NEMĚNIT !!!
        mapa[4][4]= pocatecniKarta; //kartu vkládáme do středu
        pocatecniKarta.setPolozena(true);

        //naplneni mapy objekty typu card s parametrem polozeno= false .. kvuli problemu s nullpointerem [vkladali jsme objekty do prostred prazdneho pole
        for (int i = 0; i < velikostMapy; i++) {
            System.out.println();
            for (int j = 0; j < velikostMapy; j++) {
                if (mapa[j][i] == null){
                  mapa[j][i] = new Card(false);}  // vlozeni obecne karty
                if (mapa[j][i].getPolozena() == true){
                    System.out.print("card" + " ");
                 } else System.out.print("null" + " ");
            }
        }
        Player hrac1 = new Player(7); players.add(hrac1);
        Player hrac2 = new Player(7); players.add(hrac2);
        naplneniBalikuKaret();
    }

    public boolean stranyPasuji(Segment polozenaStrana, Segment prikladanaStrana) {
        return polozenaStrana.getNazev() == prikladanaStrana.getNazev(); // ???
    }

    /**
     * @author xbobcik  @version: etapa 3
     */
    // testuje kartu nad souradnicemi [X,Y]. kartu pokladame na  [4,5], tedy testujeme [4,4]
    public boolean lzePolozitZVrchu(int x, int y, Card prikladanaKarta, Card[][] mapa) {


        if (mapa[x][y - 1].getPolozena() == false) {
            return true;
        }
        //pokud v poli nad pokladanou kartou nic neni lze ji položit. Karta nad = (y-1) kvuli tomu ze pokladame karty od prostredka do pole

        if (
                mapa[x][y - 1].getPolozena() &&
                        this.stranyPasuji(mapa[x][y - 1].getBottomSide(), prikladanaKarta.getTopSide())
        ) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * @author xbobcik  @version: etapa 3
     */
    // testuje kartu nad souradnicemi [X,Y]. kartu pokladame na  [4,5], tedy testujeme [4,4]
    public boolean lzePolozitZeSpodu(int x, int y, Card prikladanaKarta, Card[][] mapa) {

        //pokud v poli pod pokladanou kartou nic neni lze ji položit.
        if (mapa[x][y + 1].getPolozena() == false) {
            return true;
        }

        if (
                mapa[x][y + 1].getPolozena() &&
                        this.stranyPasuji(mapa[x][y + 1].getTopSide(), prikladanaKarta.getBottomSide())
        ) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * @author xbobcik  @version: etapa 3
     */
    // testuje kartu nad souradnicemi [X,Y]. kartu pokladame na  [4,5], tedy testujeme [4,4]
    public boolean lzePolozitZPrava(int x, int y, Card prikladanaKarta, Card[][] mapa) {

        //pokud v poli pod pokladanou kartou nic neni lze ji položit.
        if (mapa[x + 1][y].getPolozena() == false) {
            return true;
        }

        if (
                mapa[x + 1][y].getPolozena() &&
                        this.stranyPasuji(mapa[x + 1][y].getLeftSide(), prikladanaKarta.getRightSide())
        ) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * @author xbobcik  @version: etapa 3
     */
    // testuje kartu nad souradnicemi [X,Y]. kartu pokladame na  [4,5], tedy testujeme [4,4]
    public boolean lzePolozitZLeva(int x, int y, Card prikladanaKarta, Card[][] mapa) {

        //pokud v poli pod pokladanou kartou nic neni lze ji položit.
        if (mapa[x - 1][y].getPolozena() == false) {
            return true;
        }

        if (
                mapa[x - 1][y].getPolozena() &&
                        this.stranyPasuji(mapa[x - 1][y].getRightSide(), prikladanaKarta.getLeftSide())
        ) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * @author xbobcik  @version: etapa 3
     */
    public int pocetKaretOkolo(int x, int y, Card[][] map) {
        int pocet = 0;

        if (map[x + 1][y].getPolozena()) {
            pocet++;
        }
        if (map[x - 1][y].getPolozena()) {
            pocet++;
        }
        if (map[x][y + 1].getPolozena()) {
            pocet++;
        }
        if (map[x][y - 1].getPolozena()) {
            pocet++;
        }

        return pocet;
    }


    /**
     * @author xbobcik  @version: etapa 3
     */
//zepta se jestli je v danem okoli nejaka karta pokud ano pak se zepta jestli strany pasuji +/-
    public boolean lzePolozitKartu(int x, int y, Card prikladanaKarta, Card[][] mapa) {
        int pocetKaretOkolo = 0;
        int pocetShodujicichSeOkolo = 0;

        return (
                this.lzePolozitZVrchu(x, y, prikladanaKarta, mapa) &&
                        this.lzePolozitZeSpodu(x, y, prikladanaKarta, mapa) &&
                        this.lzePolozitZPrava(x, y, prikladanaKarta, mapa) &&
                        this.lzePolozitZLeva(x, y, prikladanaKarta, mapa) &&
                        this.pocetKaretOkolo(x, y, mapa) > 0
        );
        //return (pocetKaretOkolo == pocetShodujicichSeOkolo && pocetKaretOkolo != 0);
    }

    /**
     * @author xbobcik  @version: etapa 3
     */
//metoda vlozi kartu do mapy na souradnice X, Y prave tehdy kdyz metoda lzePolozitKartu vrati true
    public void polozKartu(int x, int y, Card prikladanaKarta, Card[][] mapa) {
        if (lzePolozitKartu(x, y, prikladanaKarta, mapa)) {
            mapa[x][y] = prikladanaKarta;
            prikladanaKarta.setPolozena(true);
        }
    }


// metoda pro polozeni figurky na kartu. parametry :cast karty - jakou cast karty polozime figurku
//
//
// / urci tridu figurky ze ktere bude vytvorena kvuli poctu bodu


    // hrac - za ktereho hrace budeme vykladat figurku

    public void polozFigurkuNaKartu(Card k, Segment castKarty, Player hrac) {
        if (
                hrac.getVylozeneFigurky().size() < hrac.MAXPOCETFIGUREK &&
                        (
                                k.getTopSide().isFigurkaPolozena() == false &&
                                        k.getBottomSide().isFigurkaPolozena() == false &&
                                        k.getLeftSide().isFigurkaPolozena() == false &&
                                        k.getRightSide().isFigurkaPolozena() == false &&
                                        k.getCenter().isFigurkaPolozena() == false
                        )
        ) {
            castKarty.setFigurkaPolozena(true);
        }
    }

    /**
     * @author xblahov1  @version: etapa 3
     */
    public Card vylosujKartu() {
        int pocetKaret = cardsInDeck.size();
        Random rnd = new Random();
        Card karta = cardsInDeck.get(rnd.nextInt(pocetKaret));
        cardsInDeck.remove(karta); // odstraneni z baliku
        return karta;
    }

    /**
     * @author xblahov1  @version: etapa 4
     */
    // LAMBDA VYRAZY PRO KONVERZI RETEZCE NA OBJEKT TYPU CARD
    static Pattern regexCard = Pattern.compile("(\\S+);(\\S+);(\\S+);(\\S+);(\\S+)");

    static Function<String, Card> stringToCard = s -> {
        Matcher m = regexCard.matcher(s); // kontrola jestli odpovida regularnimu vyrazu
        if (m.find()) {
            Segment part1 = StringToSegment(m.group(1));
            Segment part2 = StringToSegment(m.group(2));
            Segment part3 = StringToSegment(m.group(3));
            Segment part4 = StringToSegment(m.group(4));
            Segment part5 = StringToSegment(m.group(5));
            return new Card(part1, part2, part3, part4, part5);
        }
        System.out.println("neodpovida regularnimu vyrazu");
        return null;
    };

    static Consumer<Card> printCards = c -> System.out.println(c);

    Consumer<Card> addToDeck = c -> cardsInDeck.add(c);

    /** @author xblahov1  @version: etapa 4 */
    static Segment StringToSegment(String x){ // Konstruktor Card potřebuje Stringy prevest na Segmenty
        switch(x){
            case "field":
                return new Field();
            case "town":
                return new Town();
            case "route":
                return new Route();
            case "monastery":
                return new Monastery();
            default:
                return null;
        }
    }

    /**
     * @author xblahov1  @version: etapa 4
     */
    public void naplneniBalikuKaret() {
        String path = (System.getProperty("user.home") + File.separator + "init.txt");
        File f = new File(path);
        try {
            BufferedReader br = new BufferedReader(new FileReader(f)); // ziskany proud retezcu
            br.lines().map(stringToCard).forEach(addToDeck);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * @author xblahov1  @version: etapa 3
     */
    public ArrayList<Card> getCardsInDeck() { return cardsInDeck;  }




}









