package cz.mendelu.pef.pjj_2019;

public abstract class Segment {

    boolean CANBEONSIDE;
    boolean CANBEINCENTER;
    private boolean figurkaPolozena;

    public boolean isFigurkaPolozena() {
        return figurkaPolozena;
    }

    public void setFigurkaPolozena(boolean figurkaPolozena) {
        this.figurkaPolozena = figurkaPolozena;
    }




    public abstract String getNazev();


}
