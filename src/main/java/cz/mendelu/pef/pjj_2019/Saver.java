package cz.mendelu.pef.pjj_2019;

import java.io.*;

/**
 * @author xbobcik  @version: etapa 3
 */

public class Saver {
    String path = ("D:\\save.txt");

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void ulozeniStavuHry (Game hraProUlozeni){


        try {
            FileOutputStream Fos = new FileOutputStream(path);
            ObjectOutputStream OOS = new ObjectOutputStream(Fos);


            OOS.writeObject(hraProUlozeni);
/*
            OOS.writeObject(hraProUlozeni.mapa);
            OOS.writeObject(hraProUlozeni.players);
            OOS.writeObject(hraProUlozeni.getCardsInDeck());
*/
            OOS.flush();
            OOS.close();

            System.out.println("hra ulozena do souboru");


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }




    }

}
