package cz.mendelu.pef.pjj_2019;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class Loader {

    String path = ("D:\\save.txt");


    public Object nahrajRozehranouHru() {
        try {
            FileInputStream fileIn = new FileInputStream(path);
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);

            Object hra = objectIn.readObject();

            System.out.println("The Object has been read from the file");
            objectIn.close();
            return hra;


        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("nacteni hry- soubor nenalezen");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("nacteni hry- chyba IO operace");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("nacteni hry- trida nenalezena");
        }
        System.out.println("nastala chyba v nacteni souboru hry");
        return null;
    }

}








